<?php
// Enqueue custom styles and scripts
function bearsmith_enqueue_child_styles_and_scripts() {
    $dir = get_stylesheet_directory_uri();
    wp_enqueue_style( 'meet-the-moon-style', $dir . '/meet-the-moon.css', '', '' );
    wp_enqueue_script( 'meet-the-moon-scripts', $dir . '/js/meet-the-moon.js', '', '', true );
}
add_action( 'wp_enqueue_scripts', 'bearsmith_enqueue_child_styles_and_scripts', 11);