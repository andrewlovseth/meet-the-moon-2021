<?php

/*

	Template Name: Home

*/

$hero_photo = get_field('hero_photo');
$left_column_photo = get_field('left_column_photo');
$right_column_photo_1 = get_field('right_column_photo_1');
$right_column_photo_2 = get_field('right_column_photo_2');
$wide_photo = get_field('wide_photo');

get_header(); ?>

    <section class="hero-photo">
        <div class="content">
            <?php echo wp_get_attachment_image($hero_photo['ID'], 'full'); ?>
        </div>
    </section>

    <section class="main">
        <div class="description">
            <?php the_field('description'); ?>
        </div>

        <section class="photos">
            <div class="left-photo">
                <?php echo wp_get_attachment_image($left_column_photo['ID'], 'full'); ?>
            </div>

            <div class="right-photo-1">
                <?php echo wp_get_attachment_image($right_column_photo_1['ID'], 'full'); ?>
            </div>

            <div class="right-photo-2">
                <?php echo wp_get_attachment_image($right_column_photo_2['ID'], 'full'); ?>
            </div>

            <div class="bottom-photo">
                <?php echo wp_get_attachment_image($wide_photo['ID'], 'full'); ?>
            </div>
        </section>
    </section>

<?php get_footer(); ?>